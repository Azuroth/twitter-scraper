These are the libraries required to run the code:
	TextBlob
	nltk
	tweepy
	geocoder
	matplotlib
	numpy
	pandas
	re

In order for the code to execute, ensure all files are in the same folder.

The code can be run from the terminal via the command:
Linux
	python3 TwitterSearch.py

Windows (anaconda)
	python TwitterSearch.py
	
The code can also be run from an IDE from the TwitterSearch.py file.

Once executed, there will be two prompts for user input.
	The first will take term(s) from the user as a query.
	The second will take the desired sample size.
	
Please note that using a number over 1000 for the desired sample size may return a rate limit error.

If you wish to use your own twitter developer account, proceed to the TwitterCredentials file and
replace the current values with your own authentification codes.

The code used to plot the graphs/charts included in the text are from the TwitterPlots.py file
which is within the data/ folder.