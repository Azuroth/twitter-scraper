'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Plotting code seperate from TwitterScraper. 
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

import ContinentDict


#Read in the data
df1 = pd.read_csv('TweetData0.csv')
df2 = pd.read_csv('TweetData1.csv')
df3 = pd.read_csv('TweetData2.csv')

#Full outer join to match up indexes
df = df1.merge(df2, on = 'Country', how = 'outer')
df = df.merge(df3, on = 'Country', how = 'outer')

#Add specific column values in each row corresponding to polarity
df = df.fillna(0)
df['Positive'] = df['PositiveCount_x'] + df['PositiveCount_y'] + df['PositiveCount']
df['Neutral'] = df['NeutralCount_x'] + df['NeutralCount_y'] + df['NeutralCount']
df['Negative'] = df['NegativeCount_x'] + df['NegativeCount_y'] + df['NegativeCount']

#Add column containing the continent each country belongs to using imported dictionary
df['Continent'] = df['Country'].map(ContinentDict.names)

#Drop extra indexes created by outer join and the extra columns
df = df.drop(['Unnamed: 0_y', 'Unnamed: 0', 'Unnamed: 0_x', 
              'PositiveCount_x', 'PositiveCount_y', 'PositiveCount',
              'NeutralCount_x', 'NeutralCount_y', 'NeutralCount',
              'NegativeCount_x', 'NegativeCount_y', 'NegativeCount'
              ], axis = 1)

#Create new dataframe using continents instead of countries
df_con = pd.DataFrame({'Continent': df['Continent'], 'Positive': df['Positive'], 'Neutral': df['Neutral'], 'Negative': df['Negative']})
df_con = df_con.groupby('Continent').sum().reset_index()

#Split combined data to UK & Non-UK
df_uk = df[df.Country == 'United Kingdom']
df_rest = df[df.Country != 'United Kingdom']

#Pretty up the indices
df_uk = df_uk.reset_index()
df_rest = df_rest.reset_index()
df_uk = df_uk.drop(['index'], axis = 1)
df_rest = df_rest.drop(['index'], axis = 1)

df_con.to_csv('TweetData_Continent.csv')
df.to_csv('TweetData_Combined.csv')
df_uk.to_csv('TweetData_UK.csv')
df_rest.to_csv('TweetData_Rest.csv')

#Combined graph
x = np.arange(len(df['Country']))
plt.figure(figsize = (10, 11))
plt.style.use('grayscale')
plt.barh(x - 0.3, df['Positive'], height = 0.3, label = "Positive Count")        
plt.barh(x, df['Neutral'], height = 0.3, label = "Neutral Count")
plt.barh(x + 0.3, df['Negative'], height = 0.3, label = "Negative Count")
plt.xlabel('Frequency Count')
plt.ylabel('Country')
plt.title('Sentimental Analysis on Brexit According to Country.')
plt.yticks(ticks = x, labels = df['Country'])
plt.legend()
plt.savefig('BarGraph_Combined.png', bbox_inches = 'tight', pad_inches = 1)
plt.show()

#UK graph
x = np.arange(len(df_uk['Country']))
plt.style.use('grayscale')
plt.barh(x - 0.3, df_uk['Positive'], height = 0.3, label = "Positive Count")        
plt.barh(x, df_uk['Neutral'], height = 0.3, label = "Neutral Count")
plt.barh(x + 0.3, df_uk['Negative'], height = 0.3, label = "Negative Count")
plt.xlabel('Frequency Count')
plt.ylabel('Country')
plt.title('Sentimental Analysis on Brexit in the UK.')
plt.yticks(ticks = x, labels = df_uk['Country'])
plt.legend()
plt.savefig('BarGraph_UK.png', bbox_inches = 'tight', pad_inches = 1)
plt.show()

#Non-UK graph
x = np.arange(len(df_rest['Country']))
plt.style.use('grayscale')
plt.figure(figsize = (10, 11))
plt.barh(x - 0.3, df_rest['Positive'], height = 0.3, label = "Positive Count")        
plt.barh(x, df_rest['Neutral'], height = 0.3, label = "Neutral Count")
plt.barh(x + 0.3, df_rest['Negative'], height = 0.3, label = "Negative Count")
plt.xlabel('Frequency Count')
plt.ylabel('Country')
plt.title('Sentimental Analysis on Brexit Outside the UK.')
plt.yticks(ticks = x, labels = df_rest['Country'])
plt.legend()
plt.savefig('BarGraph_Rest.png', bbox_inches = 'tight', pad_inches = 1)
plt.show()

#Continent graph
x = np.arange(len(df_con['Continent']))
plt.figure(figsize = (10, 11))
plt.style.use('grayscale')
plt.barh(x - 0.3, df_con['Positive'], height = 0.3, label = "Positive Count")        
plt.barh(x, df_con['Neutral'], height = 0.3, label = "Neutral Count")
plt.barh(x + 0.3, df_con['Negative'], height = 0.3, label = "Negative Count")
plt.xlabel('Frequency Count')
plt.ylabel('Country')
plt.title('Sentimental Analysis on Brexit According to Continent.')
plt.yticks(ticks = x, labels = df_con['Continent'])
plt.legend()
plt.savefig('BarGraph_Continent.png', bbox_inches = 'tight', pad_inches = 1)
plt.show()