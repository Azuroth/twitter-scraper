'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
This file contains functionality to extract and analyze targeted data from Twitter.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

from matplotlib import *
from nltk.tokenize import word_tokenize
from textblob import TextBlob
from tweepy import OAuthHandler
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import geocoder
import re
import tweepy

#import ContinentList
import CountryList
import TwitterCredentials


#Object with various methods that retrieves different things needed by the program
class Acquirer():
    #What should we search Twitter for? Loop the question if the user enters a blank query.
    def get_keyword(self):
        while True:
            keyword = input("What topic should we analyze? ")
            #Allow for all combinations of alpha-numeric characters. Also allow a space only when it separates words.
            if re.compile(r'^\w+( \w+)*$').search(keyword):
                return keyword
    
    #How many tweets are necessary? Loop the question if the user enters any number below 1.
    def get_amount(self):
        while True:
            #Limited to most recent 1 week of data and by popularity of the topic chosen.
            amount = input("How many tweets should be extracted? ")
            if re.compile(r'\D+').search(amount):
                continue
            #Note that even though 1 is accepted here, it may not be a high enough value to get past the get_country() call.
            if int(amount) > 0:
                return int(amount)
    
    #Calculate the percentage of something using a numerator and denominator. Fails if y == 0.
    def get_percentage(self, x, y):
        return np.round(100 * float(x)/float(y), 2)

    #DOES NOT WORK. Method is meant to extract the continent code using geonames, followed by returning the full name of the continent.
#    def get_continent(self, tweet):
#        location = geocoder.geonames(tweet, key = 'Azuroth')    
#        continent = location.continentCode
#        continent = [name for code, name in ContinentList.names if continent == code]
#        return continent
    
    #Determine if the provided location is listed within the geonames database.
    def get_country(self, tweet):
        location = geocoder.geonames(tweet, key = 'Azuroth')
        #Cross-check with imported list of country names. No match == return empty string.
        if location.country not in CountryList.names:
            return ""
        else:
            return str(location.country)
    
    #Standard authorization to access twitter developer benefits. Settings are on Read-Write and values are imported from separate file.
    def get_authentication(self):
        key = OAuthHandler(TwitterCredentials.CONSUMER_KEY, TwitterCredentials.CONSUMER_SECRET)
        key.set_access_token(TwitterCredentials.ACCESS_KEY, TwitterCredentials.ACCESS_SECRET)
        return key   

    #Return a dataframe containing zipped unique entries and their frequencies from a passed-in list.
    def create_dataframe(self, data, label_x = None, label_y = None):
        arr = np.array(data)
        x, y = np.unique(arr, return_counts = True)
        df = pd.DataFrame({str(label_x): x, str(label_y): y}, index = x)
        return df
    
#Streams the most recent n amount of tweets regarding the topic of choice and filters out tweets with invalid/non-existent geolocation.
class TweetStreamer():
    def __init__(self, keywords):
        self.tweets = []
        self.acquirer = Acquirer()
        self.keywords = keywords
        self.n = self.acquirer.get_amount()
        
    def stream_tweets(self): 
        key = self.acquirer.get_authentication()
        #If rate limit is reached, pause the stream until it's okay to resume.
        api = tweepy.API(key, wait_on_rate_limit = True)
        
        try:
            #Only search for recent tweets in English. Would have liked to add until parameter but it cannot go past 7 days.
            tweets = tweepy.Cursor(api.search, q = self.keywords, lang = "en", result_type = 'recent').items(self.n)
            for tweet in tweets:
                if re.compile(r'^\w+( \w+)*$').search(tweet.user.location):
                    if re.compile(r'\S').search(self.acquirer.get_country(tweet.user.location)):
                        #Only append if the user's location: 1) exists and 2) is a real place on Earth.
                        self.tweets.append(tweet)
            return self.tweets  
        #Catch any potential errors. (Most were 401 and 429)
        except BaseException as e:
            print("Error: %s" % str(e))
        return True

#Analyzes and visualizes the data.
class TweetAnalyzer():
    def __init__(self, data, keywords):
        self.acquirer = Acquirer()
        self.tweets = []
        self.keywords = keywords
        self.t_positive = []
        self.t_neutral = []
        self.t_negative = []
        self.positive = 0
        self.neutral = 0
        self.negative = 0
        self.n = 0
        self.polarity = 0
        self.p_positive = 0
        self.p_neutral = 0
        self.p_negative = 0
        #We don't need the full json object in our list.
        for tweet in data:
            self.tweets.append([tweet.text, self.acquirer.get_country(tweet.user.location)])
    
    #This method calculates and sets the necessary values for the other methods in this class.
    def analyze_sentiment(self):       
        for text, country in self.tweets:
            temp_storage = []           
            #Normalise to all lowercase. (prevent casing issues like LOvE not being a word)
            text = text.lower()
            #Clean up for textblob sentimental analysis
            text = word_tokenize(text)
            #textblob needs a string, not an array of strings
            for word in text:
                temp_storage.append(word)
            text = ''
            text = ' '.join(str(word) for word in temp_storage)
            #Determine if the tweet has a positive/neutral/negative sentiment (eg. I dislike _____. == negative).   
            content = TextBlob(text)
            self.polarity += content.sentiment.polarity
            
            #Update polarity values and append the geolocation of the tweet to the appropriate list.
            #Unfortunately UK is considered 1 collective country. Would have liked to break it into components for analysis.
            if(content.sentiment.polarity > 0.0):
                self.positive += 1
                self.t_positive.append(country)
            elif(content.sentiment.polarity == 0.0):
                self.neutral += 1
                self.t_neutral.append(country)
            else:
                self.negative += 1
                self.t_negative.append(country)
            self.n += 1 
        
        #Calculate the percentage of tweets in each category to determine how people feel about the topic.
        self.p_positive = self.acquirer.get_percentage(self.positive, self.n)
        self.p_neutral = self.acquirer.get_percentage(self.neutral, self.n)
        self.p_negative = self.acquirer.get_percentage(self.negative, self.n)
        
        df = pd.DataFrame(data = self.tweets, columns = ['Tweet', 'Country'])
        df.to_csv('TweetContent.csv')
        
    #How do people in general feel about the topic? Visualized as a pie chart.
    def plot_sentiment(self):
        labels = ['Positive [' + str(self.p_positive) + '%]', 'Neutral [' + str(self.p_neutral) + '%]', 'Negative [' + str(self.p_negative) + '%]']
        sizes = [self.p_positive, self.p_neutral, self.p_negative]
        colors = ['green', 'blue', 'red']
        patches, texts = plt.pie(sizes, colors = colors)
        
        plt.legend(patches, labels, loc = 'best')
        plt.title('Sentimental Analysis on %s Using ' %str(self.keywords) + str(self.n) + ' Tweets.' )
        plt.axis('equal')
        plt.tight_layout()
        plt.savefig('PieChart.png', bbox_inches = 'tight', pad_inches = 1)
        plt.show()
            
    #How does each country feel about the topic? Visualized as a horizontal bar chart.
    def plot_country(self):
        x_label = 'Country'
        df_pos = self.acquirer.create_dataframe(self.t_positive, x_label, 'PositiveCount')   
        df_neu = self.acquirer.create_dataframe(self.t_neutral, x_label, 'NeutralCount')   
        df_neg = self.acquirer.create_dataframe(self.t_negative, x_label, 'NegativeCount')         
        
        #Full outer join. Missing values in this case are indicators of 0 counts.
        df = df_pos.merge(df_neu, on = 'Country', how = 'outer')
        df = df.merge(df_neg, on = 'Country', how = 'outer')
        df = df.fillna(0)
        df.to_csv('TweetData.csv')
        
        x = np.arange(len(df['Country']))
        
        plt.figure(figsize = (10, 11))
        plt.barh(x - 0.3, df['PositiveCount'], height = 0.3, label = "Positive Count", color = 'green')        
        plt.barh(x, df['NeutralCount'], height = 0.3, label = "Neutral Count", color = 'blue')
        plt.barh(x + 0.3, df['NegativeCount'], height = 0.3, label = "Negative Count", color = 'red')
        plt.xlabel('Frequency Count')
        plt.ylabel('Country')
        plt.title('Sentimental Analysis on %s According to Country.' %str(self.keywords))
        plt.yticks(ticks = x, labels = df['Country'])
        plt.legend()
        plt.savefig('BarGraph.png', bbox_inches = 'tight', pad_inches = 1)
        plt.show()
        
