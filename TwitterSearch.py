#!/usr/bin/env python
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
This file executes the functionality of the TwitterScraper.py file.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

import sys
import TwitterScraper


if __name__ == '__main__':     
    acquirer = TwitterScraper.Acquirer()
    query = acquirer.get_keyword()
    streamer = TwitterScraper.TweetStreamer(query)
    tweets = streamer.stream_tweets()
    try:
        analyzer = TwitterScraper.TweetAnalyzer(tweets, query)
        analyzer.analyze_sentiment()
        analyzer.plot_sentiment()
        analyzer.plot_country()
    except:
        sys.exit(1)